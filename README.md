# designcode_flutterapp

Desing & Code Tutorial

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

![Screen_Shot_2020-08-30_at_9.42.20_AM](/uploads/3fc772769324a668ba3980cb63758b4c/Screen_Shot_2020-08-30_at_9.42.20_AM.png)
![Screen_Shot_2020-08-30_at_9.42.50_AM](/uploads/7d540f41f5e234373e6a653b4a3f0d34/Screen_Shot_2020-08-30_at_9.42.50_AM.png)
