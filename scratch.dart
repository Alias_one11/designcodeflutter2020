
void main()
{
  //________
  String name = 'Alias';

  print('=======================\n'
        '$name\n'
        '=======================');

  List<String> hobbies = [ 'coding', 'biking', 'reading' ];

  print('=======================\n'
        '$hobbies\n'
        '=======================');

  showInfo(name: name, hobbies: hobbies);
}

//: - ∂Function
String showInfo({String name, List<String> hobbies}) {
  //________
  var infoShown = '-----------------------------------------------------------\n'
                  'My name is $name. My hobbies are $hobbies\n'
                  '-----------------------------------------------------------';
  print(infoShown);

  return infoShown;
}
