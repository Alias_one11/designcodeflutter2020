import 'package:flutter/material.dart';
import 'components/stateful_components/home_screen.dart';

Future<void> main() async => runApp(MyApp());
/*--------------------------------------------*/

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        ///: - Turns off debug mode banner
        debugShowCheckedModeBanner: false,
        //________
        ///: - HomeScreen
        home: HomeScreen(),
      );

  /// - END CLASS
}


