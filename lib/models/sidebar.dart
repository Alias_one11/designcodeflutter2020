import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../constants.dart';

class SidebarItem {
  //: - ©Global-PROPERTIES
  /*=============================*/
  String title;
  LinearGradient background;
  Icon icon;

/*=============================*/

  //: - ∂Constructor
  SidebarItem({this.title, this.background, this.icon});
}

/// - END Class

//: - ∂Sample-Data
/*--------------------------------------------*/
var sidebarItem = [
  SidebarItem(
    title: "Home",
    background: LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [myColor6, myColor, myColor3],
    ),
    icon: Icon(
      Icons.home,
      color: Colors.black,
    ),
  ),
  SidebarItem(
    title: "Courses",
    background: LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [myColor6, myColor, myColor3]),
    icon: Icon(
      Platform.isAndroid ? Icons.library_books : CupertinoIcons.book_solid,
      color: Colors.black,
    ),
  ),
  SidebarItem(
    title: "Billing",
    background: LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [myColor6, myColor, myColor3]),
    icon: Icon(
      Icons.credit_card,
      color: Colors.black,
    ),
  ),
  SidebarItem(
    title: "Settings",
    background: LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [myColor6, myColor, myColor3],
    ),
    icon: Icon(
      Platform.isAndroid ? Icons.settings : CupertinoIcons.settings_solid,
      color: Colors.black,
    ),
  ),
];
