import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../constants.dart';

class SearchFieldWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) =>
      //________
  Expanded(
    child: Padding(
      padding: EdgeInsets.only(left: 8, right: 20),
      child: Container(
        ///: - Search box decoration
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.15),
          borderRadius: BorderRadius.circular(14),
          boxShadow: [
            BoxShadow(
                color: kShadowColor,
                offset: Offset(0, 12),
                blurRadius: 16
            )
          ],
        ),
        ///: - TextField input
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: TextField(
            cursorColor: kPrimaryLabelColor,
            decoration: InputDecoration(
              ///: - Search icon
              icon: Icon(
                Icons.search,
                color: kPrimaryLabelColor,
                size: 20,
              ),
              border: InputBorder.none,
              hintText: 'Search for courses',
              hintStyle: kSearchPlaceholderStyle,
            ),
            style: kSearchTextStyle,
            //________
            onChanged: (newText) => print(newText),
          ),
        ),
      ),
    ),
  );
}
