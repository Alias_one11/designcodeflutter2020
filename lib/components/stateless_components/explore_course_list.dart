import '../../models/course.dart';
import 'package:flutter/cupertino.dart';

import '../cards/explore_course_card.dart';

class ExploreCourseList extends StatelessWidget {
  @override
  Widget build(BuildContext context) =>
      //________
  Container(
    //________
    height: 120,
    child: ListView.builder(
      //________
      scrollDirection: Axis.horizontal,
      shrinkWrap: true,
      itemCount: exploreCourses.length,
      itemBuilder: (_, index) => Padding(
        padding: EdgeInsets.only(left: index == 0 ? 20: 0),
        child: ExploreCourseCard(course: exploreCourses[index]),
      ),
    ),
  );
}
