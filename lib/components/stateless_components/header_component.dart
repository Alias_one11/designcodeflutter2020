import 'package:flutter/material.dart';
import '../../constants.dart';

class HeaderComponent extends StatelessWidget {
  const HeaderComponent({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      //: - Vertically aligned
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ///: - Title
          Text('Recent', style: kLargeTitleStyle),
          //________
          SizedBox(height: 5),
          //________
          ///: - Sub-title
          Text(
            '23 courses, more coming',
            style: kSubtitleStyle,
          ),
          //________
          SizedBox(height: 20),
        ],
      ),
    );
  }
}
