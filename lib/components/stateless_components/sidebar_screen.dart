import 'sidebar_row.dart';
import '../../constants.dart';
import '../../models/sidebar.dart';
import 'package:flutter/material.dart';

class SidebarScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) =>
      //________
      Container(
        decoration: BoxDecoration(
          gradient: myColorGradient,
          borderRadius: BorderRadius.only(topRight: Radius.circular(34)),
        ),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width * 0.85,
        padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 20),
        //: - Brings everything inside the side bar closer in
        child: SafeArea(
          //: - Vertically Stacked
        child: Column(
            children: [
              //: - Horizontally aligned
              Row(
                children: [
                  CircleAvatar(
                    backgroundImage: AssetImage('asset/images/profile.jpg'),
                    radius: 21,
                  ),
                  SizedBox(width: 15),
                  //: - Vertically aligned
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ///: - Name
                      Text('Alias One11', style: kHeadlineLabelStyle),
                      SizedBox(height: 4),
                      //________
                      ///: - License expiration
                      Text(
                        'License ends on 3 Jan, 2021',
                        style: kSearchPlaceholderStyle,
                      )
                    ],
                  )
                ],
              ),
              //: - Space between image and the top icon
              SizedBox(height: MediaQuery.of(context).size.height * 0.08),
              ///: - SidebarRow
              SidebarRow(item: sidebarItem[0]),
              //: - Space between icons
              SizedBox(height: 32),
              SidebarRow(item: sidebarItem[1]),
              //: - Space between icons
              SizedBox(height: 32),
              SidebarRow(item: sidebarItem[2]),
              //: - Space between icons
              SizedBox(height: 32),
              SidebarRow(item: sidebarItem[3]),
              //: - Space between icons
              SizedBox(height: 32),
              //________
              Spacer(),
              //________
              //: - Horizontally aligned
              Row(
                children: [
                  Image.asset('asset/icons/icon-logout.png', width: 17),
                  SizedBox(width: 12),
                  Text('Log out', style: kSecondaryCalloutLabelStyle),
                ],
              ),
            ],
          ),
        ),
      );
}
