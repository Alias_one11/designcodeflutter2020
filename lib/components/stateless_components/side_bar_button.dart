import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../constants.dart';

class SideBarButton extends StatelessWidget {
  //: - Class methods
  /*---------------------------------------------*/
  final Function triggerAnimation;

  //: - Constructor
  const SideBarButton({Key key, this.triggerAnimation}) : super(key: key);

  @override
  Widget build(BuildContext context) => RawMaterialButton(
      //: - So you only tap on the button
      //: - content & not anything outside of itself
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      onPressed: triggerAnimation,
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      constraints: BoxConstraints(maxHeight: 40, maxWidth: 40),
      //________
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(40),
            boxShadow: [
              BoxShadow(
                color: kShadowColor,
                offset: Offset(0, 12),
                blurRadius: 16,
              ),
            ]),
        //________
        child: Image.asset(
          'asset/icons/icon-sidebar.png',
          color: kPrimaryLabelColor,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 12,
          vertical: 14,
        ),
      ),
    );

  /// - END CLASS
}
