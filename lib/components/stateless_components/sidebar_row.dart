import '../../constants.dart';
import '../../models/sidebar.dart';
import 'package:flutter/cupertino.dart';

class SidebarRow extends StatelessWidget {
  //: - ©Global-PROPERTIES
  /*=============================*/
  final SidebarItem item;
  /*=============================*/

  //: - ∂Constructor
  const SidebarRow({@required this.item});

  @override
  Widget build(BuildContext context) =>
      //: - Horizontally
  Row(
    //________
    children: [
      Container(
        width: 42,
        height: 42,
        //: - padding
        padding: EdgeInsets.all(10),
        //: - Container-Background
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          //: - Gradient
          gradient: item.background,
        ),
        child: item.icon,
      ),
      //________
      SizedBox(width: 12),
      Container(
        // - Center
        child: Center(
          child: Text(
            item.title,
            style: kCalloutLabelStyle,
          ),
        ),
      ),
    ],
  );
}/// - END Widget
