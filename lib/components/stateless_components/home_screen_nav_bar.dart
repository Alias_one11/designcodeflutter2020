import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../constants.dart';
import 'search_field_widget.dart';
import 'side_bar_button.dart';

class HomeScreenNavBar extends StatelessWidget {
  //: - Class methods
  /*---------------------------------------------*/
  final Function triggerAnimation;

  //: - Constructor
  const HomeScreenNavBar({Key key, this.triggerAnimation}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      //________
      //: - Horizontally aligned
      Padding(
        padding: EdgeInsets.all(20.0),
        //: - Horizontally aligned
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ///: - SideBarButton
            SideBarButton(triggerAnimation: triggerAnimation),
            SizedBox(width: 24),

            ///: - Search bar
            SearchFieldWidget(),

            ///: - Notification bell button
            Icon(Icons.notifications, color: kPrimaryLabelColor),
            //________
            SizedBox(width: 8),
            //________
            ///: - Profile picture
            CircleAvatar(
              radius: 18,
              backgroundImage: AssetImage('asset/images/profile.jpg'),
            )
          ],
        ),
      );
}
