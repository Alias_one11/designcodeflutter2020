import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../constants.dart';
import '../../models/course.dart';

class Logo extends StatelessWidget {
  //: - ©Global-PROPERTIES
  /*=============================*/
  final Course course;
  /*=============================*/

  //: - Constructor
  const Logo({Key key, @required this.course}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 42),
      child: Container(
        child: Image.asset('asset/logos/${course.logo}'),
        height: 60,
        width: 60,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(18),
            boxShadow: [
              BoxShadow(
                color: kShadowColor,
                offset: Offset(0, 4),
                blurRadius: 16,
              ),
            ]),
        padding: EdgeInsets.all(12),
      ),
    );
  }
}
