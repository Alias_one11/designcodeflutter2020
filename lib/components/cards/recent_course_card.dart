import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../constants.dart';
import '../../models/course.dart';
import 'logo.dart';

class RecentCourseCard extends StatelessWidget {
  //: - ©Global-PROPERTIES
  /*=============================*/
  Course course;
  /*=============================*/

  //: - Constructor
  RecentCourseCard({this.course});

  @override
  Widget build(BuildContext context) =>
      //________
      Stack(alignment: Alignment.topRight, children: [
        //________
        Padding(
          padding: EdgeInsets.only(top: 20),
          child: Container(
            height: 240,
            width: 240,
            decoration: BoxDecoration(
                gradient: course.background,
                borderRadius: BorderRadius.circular(41),
                boxShadow: [
                  BoxShadow(
                      color: course.background.colors[0].withOpacity(0.5),
                      offset: Offset(0, 20),
                      blurRadius: 30),
                  BoxShadow(
                      color: course.background.colors[1].withOpacity(0.5),
                      offset: Offset(0, 20),
                      blurRadius: 30),
                ]),
            //: - Vertically aligned
            child: Column(
                //________

                children: [
                  //: - Vertically aligned
                  Padding(
                    //: - Padding of column
                    padding: EdgeInsets.only(
                      top: 32,
                      left: 32,
                      right: 32,
                    ),
                    child: Column(
                      //________
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ///: - Sub-Title
                        Text(
                          course.courseSubtitle,
                          style: kCardSubtitleStyle,
                        ),
                        //________
                        SizedBox(height: 6),
                        //________
                        ///: - Title
                        Text(
                          course.courseTitle,
                          style: kCardTitleStyle,
                        ),
                      ],
                    ),
                  ),
                  //________
                  /// - Creates a widget that expands a child of a Row, Column,
                  /// - or Flex so that the child fills the available space
                  /// - along the flex widget's main axis.
                  Expanded(
                    child: Image.asset(
                      'asset/illustrations/${course.illustration}',
                      fit: BoxFit.cover,
                    ),
                  )
                ]),
          ),
        ),

        ///: - Logo
        Logo(course: course),
      ]);
}


