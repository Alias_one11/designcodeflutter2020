import 'package:designcode_flutterapp/models/course.dart';
import '../../constants.dart';
import 'package:flutter/cupertino.dart';

class ExploreCourseCard extends StatelessWidget {
  //: - ©Global-PROPERTIES
  /*=============================*/
  final Course course;

  /*=============================*/

  //: - Constructor
  const ExploreCourseCard({Key key, this.course}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      //________
  Padding(
    padding: EdgeInsets.only(right: 20),
    //________
    child: ClipRRect(
      borderRadius: BorderRadius.circular(41),
      //________
      child: Container(
        //________
        height: 120,
        width: 280,
        decoration: BoxDecoration(gradient: course.background),
        child: Padding(
          padding: EdgeInsets.only(left: 32),

          ///: - Horizontally aligned
          child: Row(
            children: [
              //________
              Expanded(
                ///: - Vertically aligned
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //________
                  children: [
                    //________
                    ///: - Course sub title
                    Text(course.courseSubtitle, style: kCardSubtitleStyle),
                    SizedBox(height: 6),

                    ///: - Course title
                    Text(course.courseTitle, style: kCardTitleStyle),
                  ],
                ),
              ),
              //________
              ///: - Vertically aligned
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                //________
                children: [
                  ///: - Image
                  Image.asset(
                    'asset/illustrations/${course.illustration}',
                    fit: BoxFit.cover,
                    height: 100,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
