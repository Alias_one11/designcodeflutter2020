import '../../components/cards/recent_course_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../models/course.dart';

class _RecentCourseListState extends State<RecentCourseList> {
  //: - ©Global-PROPERTIES
  /*=============================*/
  List<Container> indicators = [];
  var currentPage = 0;

  //: - Class methods
  /*=============================*/
  Widget updateIndicators() =>
      //________
      //: - Horizontally aligned
      Row(
          mainAxisAlignment: MainAxisAlignment.center,
          //________
          children: recentCourses.map((course) {
            //________
            var index = recentCourses.indexOf(course);
            //________
            return Container(
              width: 7,
              height: 7,
              margin: EdgeInsets.symmetric(horizontal: 6),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: currentPage == index
                      ? Colors.lightBlueAccent
                      : Colors.blueGrey),
            );
          }).toList());

  @override
  Widget build(BuildContext context) =>
      //________
//: - Vertically aligned
      Column(
        children: [
          Container(
            height: 320,
            width: double.infinity,
            child: PageView.builder(
              itemBuilder: (_, index) => Opacity(
                opacity: currentPage == index ? 1 : 0.4,
                //________
                child: RecentCourseCard(
                  course: recentCourses[index],
                ),
              ),
              //________
              itemCount: recentCourses.length,
              //: - Adds spacing for the cards
              controller:
                  PageController(initialPage: 0, viewportFraction: 0.63),
              onPageChanged: (index) => setState(
                () => currentPage = index,
              ),
            ),
          ),
          //: - updateIndicators
          updateIndicators(),
        ],
      );

/// - END CLASS
}

class RecentCourseList extends StatefulWidget {
  @override
  _RecentCourseListState createState() => _RecentCourseListState();
}
