import './../stateless_components/home_screen_nav_bar.dart';
import '../stateless_components/sidebar_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../constants.dart';
import '../stateless_components/explore_course_list.dart';
import '../stateless_components/header_component.dart';
import 'recent_course_list.dart';

class _HomeScreenState extends State<HomeScreen>
    with TickerProviderStateMixin {
  //: - ©Global-PROPERTIES
  /*=============================*/
  Animation<Offset> sidebarAnimation;
  Animation<double> fadeAnimation;
  AnimationController sidebarAnimationController;
  bool sidebarHidden = true;
  /*=============================*/

  //: - Init
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //________
    ///: - sidebarAnimationController
    sidebarAnimationController = AnimationController(
      //________
      vsync: this,
      duration: Duration(milliseconds: 250),
    );

    ///: - sidebarAnimation
    sidebarAnimation = Tween<Offset>(
      //: - -1 at the `x` position hides the
      //: - side bar in its initial state
      begin: Offset(-1, 0),
      end: Offset(0, 0),
    ).animate(
      ///: - CurvedAnimation
      CurvedAnimation(
        parent: sidebarAnimationController,
        curve: Curves.easeInOut,
      ),
    );

    fadeAnimation = Tween<double>(
      //: - begins at `0` opacity
      begin: 0,
      //: - ends at `1` or full opacity
      end: 1,
    ).animate(
      ///: - CurvedAnimation
      CurvedAnimation(
        parent: sidebarAnimationController,
        curve: Curves.easeInOut,
      ),
    );

    /// - END initState
  }

  //: - The dispose method prevents memory
  //: - leaks when the state is discarded
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    sidebarAnimationController.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        //: - Home-Icon
        body: Container(
          ///: - Background gradient
          decoration: BoxDecoration(gradient: myColorGradient),
          //________
          ///: - Stacked on top of each view
          child: Stack(
              //________
              children: [
                SafeArea(
                  //________
                  child: Column(
                      //________
                      children: [
                        ///: - HomeScreenNavBar
                        HomeScreenNavBar(
                          triggerAnimation: () {
                            setState(() => sidebarHidden = false);
                            return sidebarAnimationController.forward();
                          },
                        ),
                        //________
                        ///: - HeaderComponent
                        HeaderComponent(),
                        //________
                        ///: - RecentCourseList
                        RecentCourseList(),
                        //________
                        paddingBetweenTopBottom,
                        //________
                        ///: - ExploreCourseList
                        ExploreCourseList(),
                      ]),
                ),
                //________
                IgnorePointer(
                  ///: - Ignores or excepts taps
                  ignoring: sidebarHidden,
                  //________
                  ///: - ZStacked on top of each other
                  child: Stack(
                      //________
                      children: [
                        FadeTransition(
                          //________
                          opacity: fadeAnimation,
                          child: GestureDetector(
                            //________
                            onTap: () {
                              setState(() => sidebarHidden = false);
                              return sidebarAnimationController.reverse();
                            },
                            //________
                            child: Container(
                              //________
                              color: Color.fromRGBO(36, 38, 41, 0.4),
                              height: MediaQuery.of(context).size.height,
                              width: MediaQuery.of(context).size.width,
                            ),
                          ),
                        ),

                        ///: - Animation-->SlideTransition
                        SlideTransition(
                          position: sidebarAnimation,
                          //________
                          child: SafeArea(
                            bottom: false,
                            //________
                            ///: - SideBarScreen
                            child: SidebarScreen(),
                          ),
                        ),
                      ]),
                ),
              ]),
        ),
      );

  //: - Helper class methods
  /*---------------------------------------------*/
  Padding get paddingBetweenTopBottom => Padding(
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
          top: 25,
          bottom: 16,
        ),

        ///: - Vertically aligned
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('Explore', style: kTitle1Style),
          ],
        ),
      );

  ///: - END CLASS
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}
